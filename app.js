const getLoanButton = document.getElementById("loanButton");
const balanceParagraph = document.getElementById("balance");
const balance = balanceParagraph.innerHTML.replace("€ ", "");
const bankButton = document.getElementById("transferMoney");
const workButton = document.getElementById("work");
const payBalanceParagraph = document.getElementById("payBalance");
const computerOptions = document.getElementById("dropdownMenu");
const computerSpecs = document.getElementById("computerInfo");
const computerImg = document.getElementById("computerImg");
const computerTitle = document.getElementById("computerTitle");
const computerDescription = document.getElementById("computerDescription");
const computerPrice = document.getElementById("price");
const buyNowButtom = document.getElementById("buyNowButtom");

let isLoanPossible = true;
let computers = [];

//enter an amount when "Get Loan" button is clicked
getLoanButton.addEventListener("click", function () {
  const value = parseInt(prompt("Enter with ammount of the loan."));

  //verify if loan is more than double of your bank balance
  if (value !== null && value < 2 * balance) {
    //verify if you get more than one bank loan before buying a computer
    if (!isLoanPossible) {
      alert("You cannot get more than one bank loan before buying a computer.");
      return;
    }

    //adding outstanding amount to bank balance
    let balanceParagraph = document.getElementById("balance");
    let balance = parseInt(balanceParagraph.innerHTML.replace("€ ", ""));
    balanceParagraph.innerText = "€ " + (balance + value);

    const outstandingRow = document.getElementById("outstandingRow");

    let para = document.getElementById("outstandingPara");
    if (!para) {
      para = document.createElement("p");
      para.id = "outstandingPara";
      para.className = "col";
      para.innerText = "Outstanding Loan";
      outstandingRow.appendChild(para);
    }

    //Creating outstanding balance
    let paraValue = document.getElementById("outstandingBalance");
    if (!paraValue) {
      paraValue = document.createElement("p");
      paraValue.id = "outstandingBalance";
      paraValue.className = "col";
      paraValue.innerText = "€ " + value;
      outstandingRow.appendChild(paraValue);
    } else {
      paraValue.innerText =
        "€ " + (parseInt(paraValue.innerText.replace("€ ", "")) + value);
    }

    //Creating "pay" button
    let loanPayButton = document.getElementById("payButton");
    if (!loanPayButton) {
      loanPayButton = document.createElement("button");
      loanPayButton.className = "btn btn-danger col";
      loanPayButton.innerText = "Pay";
      loanPayButton.id = "payButton";
      const buttonRow = document.getElementById("buttonRow");
      buttonRow.appendChild(loanPayButton);
    }

    isLoanPossible = false; //is not possible to have another loan before buying a laptop

    loanPayButton.addEventListener("click", function () {
      let currentPayBalance = parseInt(
        payBalanceParagraph.innerHTML.replace("€ ", "")
      );
      let outstandingBalanceParagraph =
        document.getElementById("outstandingBalance");
      let outstandingBalance = parseInt(
        outstandingBalanceParagraph.innerHTML.replace("€ ", "")
      );
      // if outstanding balance amount is bigger than current pay balance,   se debito eh maior que salario, reset salary amount and reduce outstanding balance amount
      if (outstandingBalance > currentPayBalance) {
        outstandingBalance = outstandingBalance - currentPayBalance;
        currentPayBalance = 0;
        // if outstanding balance amount is less than salary, reset debit and decrease salary
      } else if (outstandingBalance < currentPayBalance) {
        currentPayBalance = currentPayBalance - outstandingBalance;
        outstandingBalance = 0;
        //if outstanding balance amount is equal to salary
      } else {
        currentPayBalance = 0;
        outstandingBalance = 0;
      }
      payBalanceParagraph.innerHTML = "€ " + currentPayBalance;
      outstandingBalanceParagraph.innerHTML = "€ " + outstandingBalance;
    });
  }
});

//add 100 euros to pay balance every time you click on "work" button
workButton.addEventListener("click", function () {
  let workValue = parseInt(payBalanceParagraph.innerHTML.replace("€ ", ""));
  workValue += 100;
  payBalanceParagraph.innerHTML = "€ " + workValue;
});

//tranfering the money from your Pay balance to your Bank balance
bankButton.addEventListener("click", function () {
  let payBalance = parseInt(payBalanceParagraph.innerHTML.replace("€ ", ""));
  let currentBalance = parseInt(balanceParagraph.innerHTML.replace("€ ", ""));
  let outstandingBalanceParagraph =
    document.getElementById("outstandingBalance");

  // if you have an outstanding loan, 10% of your salary MUST first be deducted and transferred to the outstanding Loan amount
  if (outstandingBalanceParagraph) {
    let outstandingBalance = parseInt(
      outstandingBalanceParagraph.innerHTML.replace("€ ", "")
    );
    let payBalanceDiscount = payBalance * 0.1;
    let newBalance = currentBalance + payBalance - payBalanceDiscount;
    balanceParagraph.innerHTML = "€ " + newBalance.toFixed(2);
    let newOutstandingBalance = outstandingBalance - payBalanceDiscount;
    outstandingBalanceParagraph.innerHTML =
      "€ " + newOutstandingBalance.toFixed(2);
    //The balance after the 10% deduction may be transferred to your bank account
  } else {
    let newBalance = payBalance + currentBalance;
    balanceParagraph.innerHTML = "€ " + newBalance;
  }
  payBalanceParagraph.innerHTML = "€ 0";
});

//API endpoint
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((data) => (computers = data))
  .then((computers) => addComputersToMenu(computers));

//Adding computers from RESTful API to dropdown menu
const addComputersToMenu = (computers) => {
  computers.forEach((element) => addComputerToMenu(element));
  loadComputer(computers[0]); //loading the first computer after the API is called.
};

//adding an option to the computer seclected list
const addComputerToMenu = (computer) => {
  const computerElement = document.createElement("option");
  computerElement.value = computer.id;
  computerElement.appendChild(document.createTextNode(computer.title));
  computerOptions.appendChild(computerElement);
};

//loading the computer after it is selected
const handleComputerMenuChange = (e) => {
  const selectedComputer = computers[e.target.selectedIndex];
  loadComputer(selectedComputer);
};

//manipulating data from the chosen computer
const loadComputer = (selectedComputer) => {
  computerSpecs.innerHTML = selectedComputer.specs.join("</br>"); //joining elements of array in a single string separeted by a line break;
  computerTitle.innerText = selectedComputer.title;
  computerDescription.innerText = selectedComputer.description;
  computerPrice.innerText = "€ " + selectedComputer.price;
  computerImg.src =
    "https://noroff-komputer-store-api.herokuapp.com/" + selectedComputer.image;
};

computerOptions.addEventListener("change", handleComputerMenuChange);

// buyNow button will attempt to “Buy” a laptop
buyNowButtom.addEventListener("click", function () {
  let balanceParagraph = document.getElementById("balance");
  let balance = parseInt(balanceParagraph.innerHTML.replace("€ ", ""));
  let computerPrice = document.getElementById("price");
  let price = parseInt(computerPrice.innerHTML.replace("€ ", ""));
  //If you have enough money in the “Bank", the amount must be deducted from the bank
  if (balance >= price) {
    balance = balance - price;
    balanceParagraph.innerHTML = "€ " + balance;
    isLoanPossible = true;
    alert("You are now the owner of the new laptop!");
  } else {
    // If you do not have enough money in the “Bank”
    alert("You cannot afford the laptop.");
  }
});
